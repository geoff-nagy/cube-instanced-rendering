// Cube Instanced Rendering
// Geoff Nagy
// Demonstrates how to render a ton of cubes using instanced rendering.

// Use the mouse (click-and-drag) to look around, and the mouse wheel to zoom in and out.

#include "rendering/cubes.h"						// the cube-of-cubes to render

#include "gl/glew.h"								// OpenGL extension wrangler, otherwise we only get GL 1.1 or so
#include "glfw/glfw3.h"								// context, window, and input handling
#include "glm/glm.hpp"								// OpenGL Mathematics library to handle our vector and matrix math
#include "glm/gtc/matrix_transform.hpp"				// for lookAt() and perspective(), since we shouldn't use GLU for these (GLU is deprecated)
#include "glm/gtx/rotate_vector.hpp"				// used to compute camera viewing angles when rotating around the center of the world
using namespace glm;

#include <cstdlib>
#include <iostream>
using namespace std;

// GLFW window and characteristics
GLFWwindow *window;
vec2 windowSize;

// camera position
float cameraDistance, cameraAngleX, cameraAngleY;
vec3 cameraPos;

// track old mouse position for mouse control of camera
double oldMouseX, oldMouseY;

// properties of the perspective view that we use for rendering everything else
mat4 perspectiveProjection;
mat4 model;
mat4 perspectiveView;

// intialization functions
void openWindow();
void prepareOpenGL();
void prepareViewingParams();

// camera control
void mouseScrollCallback(GLFWwindow* window, double x, double y);
void moveCameraWithMouse();
void setCameraPosition(float distance, float angleX, float angleY);

int main(int args, char *argv[])
{
	const int NUM_CUBES = 50;				// how many cubes we want in our cube-of-cubes
	const float CUBE_SPACING = 1.5;			// how far apart the cubes should be

	// position and orientation of cubes; this doesn't change but it's good practice to define it anyways
	// in case we want to change it during the course of the program
	mat4 cubesPos = mat4(1.0);

	// our objects to render
	Cubes *cubes;

	// create our OpenGL window and context, and set some rendering options
	openWindow();
	prepareOpenGL();
	prepareViewingParams();

	// initialize our graphical objects
	cubes = new Cubes(NUM_CUBES, CUBE_SPACING);

	while(!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
	{
		// clear our colour buffer; there's no need to clear the depth buffer since we don't use it
		glClear(GL_COLOR_BUFFER_BIT);

		// update the camera position from mouse input
		moveCameraWithMouse();
		setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);

		// render our sexy cubes
		cubes -> render(perspectiveProjection, cubesPos, perspectiveView);

		// get input events and update our framebuffer
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	// shut down GLFW
	glfwDestroyWindow(window);
	glfwTerminate();
}

void openWindow()
{
	const int WIDTH = 800;
	const int HEIGHT = 600;
	const char *TITLE = "Cube Instanced Rendering";
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	if(!glfwInit())
	{
		cerr << "openWindow() could not initialize GLFW" << endl;
		exit(1);
	}

	// explicitly set our OpenGL context to something that doesn't support any old-school shit; GL 3.2 core is recent enough
	// to satisfy that constraint, but has also been around long enough that everyone supports it
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);

	// create our OpenGL window using GLFW
    window = glfwCreateWindow(WIDTH, HEIGHT,					// specify width and height
							  TITLE,							// title of window
							  NULL,								// if NULL, windowed mode (otherwise fullscreen)
							  NULL);							// not sharing resources across monitors
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// configure our viewing area
	glViewport(0, 0, WIDTH, HEIGHT);

	// enable our extensions handler
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		cerr << glewGetErrorString(error) << endl;
		exit(1);
	}

	// clear the OpenGL error code that results from initializing GLEW (another GLEW bug, I guess...)
	glGetError();

	// use a background color we'll recognize
	glClearColor(0.0, 0.0, 0.0, 1.0);

	// save our window size for later on
	windowSize = vec2(WIDTH, HEIGHT);

	// print our OpenGL version info
    cout << "-- GL version:   " << (char*)glGetString(GL_VERSION) << endl;
    cout << "-- GL vendor:    " << (char*)glGetString(GL_VENDOR) << endl;
    cout << "-- GL renderer:  " << (char*)glGetString(GL_RENDERER) << endl;
	cout << "-- GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}

void prepareOpenGL()
{
	// turn off depth testing and enable additive blending
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// black screen in background
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void prepareViewingParams()
{
	const float FOV = 45.0f;
	const float ASPECT_RATIO = (float)windowSize.x / (float)windowSize.y;

	const float PERSPECTIVE_NEAR_RANGE = 0.01f;						// how close we can see something
	const float PERSPECTIVE_FAR_RANGE = 5000.0f;					// how far away we can see something

	const float STARTING_CAMERA_DISTANCE = 120.0f;
	const float STARTING_CAMERA_ANGLE_X = -M_PI / 10.0;
	const float STARTING_CAMERA_ANGLE_Y = -M_PI / 2.0;

	// compute our perspective projection---this never changes so we can just do it once
	perspectiveProjection = perspective(FOV, ASPECT_RATIO, PERSPECTIVE_NEAR_RANGE, PERSPECTIVE_FAR_RANGE);

	// set up our mouse scroll callback; mouseScrollCallback() will be called whenever the mouse is scrolled
	glfwSetScrollCallback(window, mouseScrollCallback);

	// position our camera somewhere sane to start with
	cameraDistance = STARTING_CAMERA_DISTANCE;
	cameraAngleX = STARTING_CAMERA_ANGLE_X;
	cameraAngleY = STARTING_CAMERA_ANGLE_Y;
	setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);
}

void mouseScrollCallback(GLFWwindow* window, double x, double y)
{
	const float SCROLL_SPEED = 4.0;				// meters we scroll per scroll notch
	const float MIN_DIST = 4.0;					// how close we're allowed to get
	const float MAX_DIST = 300.0;				// how far we're allowed to get

	// adjust the camera distance either forwards or backwards, and keep it within a reasonable distance
	cameraDistance -= (y > 0.0 ? SCROLL_SPEED : -SCROLL_SPEED);
	if(cameraDistance > MAX_DIST) cameraDistance = MAX_DIST;
	if(cameraDistance < MIN_DIST) cameraDistance = MIN_DIST;
}

void moveCameraWithMouse()
{
	const float MAX_ANGLE = (M_PI / 2.0f) - 0.001;
	double mouseX, mouseY;

	// always update our mouse position
	glfwGetCursorPos(window, &mouseX, &mouseY);

	// only do something if we detect a mouse button press
	if(glfwGetMouseButton(window, 0))
	{
		cameraAngleX -= (mouseY - oldMouseY) * 0.01;
		cameraAngleY -= (mouseX - oldMouseX) * 0.01;
		if(cameraAngleX > MAX_ANGLE) cameraAngleX = MAX_ANGLE;
		if(cameraAngleX < -MAX_ANGLE) cameraAngleX = -MAX_ANGLE;
	}

	// track our old mouse position for mouse movement calculations next frame
	oldMouseX = mouseX;
	oldMouseY = mouseY;
}

void setCameraPosition(float distance, float angleX, float angleY)
{
	const vec3 CAMERA_LOOK(0.0, 0.0, 0.0);		// look at center position
	const vec3 CAMERA_UP(0.0, 1.0, 0.0);		// define what direction the camera considers to be 'up'

	// position our camera around the center point based on the incoming distance and viewing angle
	cameraPos = vec3(0.0, 0.0, cameraDistance);
	cameraPos = rotate(cameraPos, angleX, vec3(1.0, 0.0, 0.0));
	cameraPos = rotate(cameraPos, angleY, vec3(0.0, 1.0, 0.0));

	// compute a matrix describing the position and orientation of our camera
	perspectiveView = lookAt(cameraPos, CAMERA_LOOK, CAMERA_UP);
}
