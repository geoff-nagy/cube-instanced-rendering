#pragma once

#include "gl/glew.h"
#include "glm/glm.hpp"

class Shader;

class Cubes
{
private:
	GLuint vao;				// identifier for the GL object that contains our vertex attribute states
	GLuint vbos[4];			// identifiers for the GL vertex buffers we use to render the cubes (vertices, positions, colours, indices)

	Shader *shader;			// since we're using GL 3.2, we must provide our own shaders since fixed-functionality is disabled

	int numIndices;			// number of indices per cube
	int numPrimitives;		// how many cubes will be in our cube-of-cubes
	int numColors;			// how many unique colours we have; this should be equal to numPrimitives in this case

	void setupVBOs(int count, float spacing);			// set aside and configure some of the memory we'll need to pass into the GPU
	void setupShader();									// load up and compile our shader object

public:
	// count: number of cubes to make inside our cube-of-cubes
	// spacing: how spread out the cubes will be from each other
	Cubes(int count, float spacing);
	~Cubes();

	// renders the cubes using the given viewing parameters
	// projection: the type of camera projection used (e.g., orthographic, perspective)
	// model: defines how the object should be transformed
	// view: the position and orientation of the camera
	void render(glm::mat4 &projection, glm::mat4 &model, glm::mat4 &view);
};
