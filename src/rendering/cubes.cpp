#include "rendering/cubes.h"

#include "util/shader.h"

#include "gl/glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

Cubes::Cubes(int count, float spacing)
{
    setupVBOs(count, spacing);
    setupShader();
}

Cubes::~Cubes()
{
	delete shader;
}

// render cube as triangle strips, using the approach outlined in: http://www.cs.umd.edu/gvil/papers/av_ts.pdf
// prepare cube geometry, and set things up for instanced rendering
void Cubes::setupVBOs(int count, float spacing)
{
	const int NUM_VERTICES = 8;
	const GLfloat VERTICES[] = {-0.5, -0.5, 0.5,		// 0 front half
								-0.5, 0.5, 0.5,			// 1
								0.5, 0.5, 0.5,			// 2
								0.5, -0.5, 0.5,			// 3
								-0.5, -0.5, -0.5,		// 4 back half
								-0.5, 0.5, -0.5,		// 5
								0.5, 0.5, -0.5,			// 6
								0.5, -0.5, -0.5};		// 7

	// use the triangle strip approach outlined in http://www.cs.umd.edu/gvil/papers/av_ts.pdf
	// since it's more efficient than drawing separate triangles
	numIndices = 14;
	const GLuint INDICES[] = {1, 2, 0, 3, 7, 2, 6, 1, 5, 0, 4, 7, 5, 6};

	// our positions
	numPrimitives = (count * count * count);
	GLfloat *positions = new GLfloat[numPrimitives * 3];

	// our colors
	numColors = numPrimitives;
	GLfloat *colors = new GLfloat[numColors * 3];

	// position our objects in 3D space and colour them based on their positions
	GLfloat *pos = positions;
	GLfloat *color = colors;
	double x, y, z;
	for(x = 0; x < count; x ++)
	{
		for(y = 0; y < count; y ++)
		{
			for(z = 0; z < count; z ++)
			{
				*pos++ = (x - count / 2) * spacing;
				*pos++ = (y - count / 2) * spacing;
				*pos++ = (z - count / 2) * spacing;

				*color++ = (float)x / (float)count;
				*color++ = (float)y / (float)count;
				*color++ = (float)z / (float)count;
			}
		}
	}

	// create our vertex array object and our vertex buffer objects
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(4, vbos);

	// create and populate the buffer that holds our vertices
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * NUM_VERTICES * 3, VERTICES, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	// create and populate the buffer that holds our object positions
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numPrimitives * 3, positions, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glVertexAttribDivisor(1, 1);
	glEnableVertexAttribArray(1);

	// create and populate the buffer that holds our object colors
	glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numColors * 3, colors, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glVertexAttribDivisor(2, 1);
	glEnableVertexAttribArray(2);

	// create and populate the buffer that holds our indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * numIndices, INDICES, GL_STATIC_DRAW);
}

void Cubes::setupShader()
{
	shader = new Shader("shaders/cube.vert", "shaders/cube.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> bindAttrib("a_Position", 1);
	shader -> bindAttrib("a_Color", 2);
	shader -> link();
	shader -> bind();
}

void Cubes::render(mat4 &projection, mat4 &model, mat4 &view)
{
	// activate our shader and pass in the viewing params
	shader -> bind();
	shader -> uniformMatrix4fv("u_Projection", 1, value_ptr(projection));
	shader -> uniformMatrix4fv("u_Model", 1, value_ptr(model));
	shader -> uniformMatrix4fv("u_View", 1, value_ptr(view));

	// bring in our GL vertex attribute state
	glBindVertexArray(vao);
	glDrawElementsInstanced(GL_TRIANGLE_STRIP, numIndices, GL_UNSIGNED_INT, NULL, numPrimitives);
}
