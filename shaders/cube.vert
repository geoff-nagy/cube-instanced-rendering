#version 150

uniform mat4 u_Projection;		// camera projection (e.g., orthographic, perspective)
uniform mat4 u_Model;			// how this vertex should be transformed
uniform mat4 u_View;			// position and orientation of camera

in vec3 a_Vertex;				// position of vertex
in vec3 a_Position;				// position of primitive (since we're using instanced rendering)
in vec3 a_Color;				// color of primitive (since we're using instanced rendering)

out vec3 v_Color;				// outgoing color, being passed to fragment shader after interpolation

void main()
{
	// pass the outgoing vertex color to the fragment shader
	v_Color = a_Color;

	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_Projection * u_Model * u_View * vec4(a_Vertex + a_Position, 1.0);
}
